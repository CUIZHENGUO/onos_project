#!/bin/sh

echo ">> install build-essential >>"
apt-get install -y -y --force-yes build-essential
sleep 1;
echo ">> install libncurses5-dev>>"
apt-get install -y --force-yes libncurses5-dev
sleep 1;
echo ">> install libssl-dev>>"
apt-get install -y --force-yes libssl-dev
sleep 1;
echo ">> install automake>>"
apt-get install -y --force-yes automake
sleep 1;
echo ">> install libtool>>"
apt-get install -y --force-yes libtool
sleep 1;
echo ">> install xutils-dev>>"
apt-get install -y --force-yes xutils-dev
sleep 1;
echo ">> install libevent-dev>>"
apt-get install -y --force-yes libevent-dev
sleep 1;
echo ">> install libperl-dev for snmp>>"
apt-get install -y --force-yes libperl-dev
sleep 1;
echo ">> install libsnmp-perl for snmp>>"
apt-get install -y --force-yes libsnmp-perl
sleep 1;
echo ">> install dhcp3-server for accessd>>"
apt-get install -y --force-yes dhcp3-server
sleep 1;
echo ">> install bison for libnl>>"
apt-get install -y --force-yes bison
sleep 1;
echo ">> install flex for libnl>>"
apt-get install -y --force-yes flex
sleep 1;

echo ">> -------------------->>"
echo ">> install libnl-cli-3-dev with source (libnl-3.2.24)>>"
wget https://www.infradead.org/~tgr/libnl/files/libnl-3.2.24.tar.gz
tar xvzf libnl-3.2.24.tar.gz
cd libnl-3.2.24
./configure
make all install
sleep 1;
cd -
rm -rf libnl-3.2.24 libnl-3.2.24.tar.gz
sleep 1;

echo ">> -------------------->>"
echo ">> install libjansson-dev with source (jansson-2.5)>>"
wget http://www.digip.org/jansson/releases/jansson-2.5.tar.gz
tar zxvf jansson-2.5.tar.gz
cd jansson-2.5
./configure
make all install
sleep 1;
cd -
rm -rf jansson-2.5 jansson-2.5.tar.gz
sleep 1;

echo ">> install libevent>>"
apt-get install -y --force-yes libevent-dev
sleep 1;

echo ">> install python-dev>>"
apt-get install -y --force-yes python-all-dev
sleep 1;

echo ">> install python-setuptools>>"
apt-get install -y --force-yes python-setuptools
sleep 1;

easy_install greenlet
sleep 1;

easy_install gevent
sleep 1;

apt-get install -y --force-yes python-pip
sleep 1;

pip install web.py
sleep 1;

pip install bottle
sleep 1;

pip install jsonschema
sleep 1;

apt-get install -y --force-yes dos2unix
sleep 1;

easy_install netifaces
sleep 1;

apt-get install -y --force-yes libffi-dev
sleep 1;

pip install cryptography
sleep 1;

pip install paramiko
sleep 1;

pip install psutil
