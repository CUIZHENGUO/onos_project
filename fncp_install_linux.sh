#!/bin/bash

what_em=$(hostname)

#cp /usr/local/etc/MSD/procmgr.conf.$what_em /usr/local/etc/MSD/procmgr.conf

#if [ what_em=="l3" ] || [ what_em=="vnf_l3"] || [ what_em=="vnf-l3"]; then
#    cp /usr/local/etc/MSD/procmgr.conf.l3 /usr/local/etc/MSD/procmgr.conf
#fi
#if [ what_em=="l2" ] || [ what_em=="vnf_l2"] || [ what_em=="vnf-l2"]; then
#    cp /usr/local/etc/MSD/procmgr.conf.l2 /usr/local/etc/MSD/procmgr.conf
#fi


#cd /root
#svn co http://n2os.etri.re.kr:8080/svn/fncp-project/trunk/fncp-0.01.00

sleep 1
cd /root/fncp-0.01.00/vnfs/n2osVnf/n2os
make; make install
sleep 1
ldconfig -v

sleep 1
cd /root/fncp-0.01.00/vnfs/n2osVnf/
cp -r em /usr/local/sbin/ 
cp -r service_em_linux.sh /usr/local/sbin/em
sleep 1

ln /usr/local/sbin/cmsh /usr/sbin/cmsh

ln /usr/local/sbin/em/service_em_linux.sh /etc/init.d/em

cd /etc/init.d

#update-rc.d em defaults
#update-rc.d -f em remove
update-rc.d em defaults 99 99
#update-rc.d em start runlvl 4

