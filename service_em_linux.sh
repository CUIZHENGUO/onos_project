#!/bin/sh
### BEGIN INIT INFO
# Provides:          <NAME>
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# chkconfig: 234 90 90
# Description:       em
### END INIT INFO

#SCRIPT=<COMMAND>
#RUNAS=<USERNAME>

pid=`ps -ef |grep "emMain.py" | grep -v 'grep' | awk '{print $2}'`

start() {
  if [ -z $pid ]; then
    cd /usr/local/sbin/em
    ./run.sh
    echo 'em started' >&2
  else
    echo 'em aleady running' >&2
    return 1
  fi

}

stop() {
  if [ -z $pid ]; then
    echo 'em not running' >&2
    return 1
  fi
  echo 'Stopping em' >&2
  kill -9 $pid
  echo 'em stopped' >&2
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart)
    stop
    pid=`ps -ef |grep "emMain.py" | grep -v 'grep' | awk '{print $2}'`
    start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
esac
