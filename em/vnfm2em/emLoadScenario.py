import json
import os
import sys
from em2vnmf import emSetting
from vnfm2em.emStop import emStopVnfNone
from vnfm2em.emStart import emStartVnfNone
from util.emLog import getLog
from util.emGetPut import emGetPutVnf


def loadScenario(data):
    log_rev = "****************************************\n"
    log_rev += "VNF Load Scenario - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    data_rev = {}
    data_rev['id'] = data['id']
    data_rev['sync'] = data['sync']
    data_rev['file'] = []
    for parameter in data['parameters']:
        index = parameter['index']
        check_file = False
        for item in parameter['varargs']:
            if item['name'] == 'repositoryInfo':
                data_rev['repositoryInfo'] = item['value']
            elif item['name'] == 'configPath':
                configPath = item['value']
            elif item['name'] == 'configFileName':
                check_file = True
                configFileName = item['value']
        if check_file is True:
            data_rev['file'].append((configPath, configFileName))
    
    try:
        emSetting.emlog.saveLog(getLog('VNF Load Scenario.........'))
        if len(emSetting.DEFAULT_SAVE_DIR) == 0:
            return 'Fail', "Can't find default config folder"
        i = 0
        for file_info in data_rev['file']:
            configPath = file_info[0]
            configFileName = file_info[1]
            if configFileName is None or configFileName is '':
                continue

            if configFileName == '*':            
                localpath = emSetting.DEFAULT_SAVE_DIR[i]
                #emSetting.LOAD_SCENARIO_PATH = localpath
                emSetting.emlog.saveLog(getLog("Download all from (%s - %s)" % (configPath, data_rev['repositoryInfo']), True))
                res = emGetPutVnf(localpath=localpath,
                                  remotepath=configPath,
                                  method='downloadall',
                                  repository=data_rev['repositoryInfo']
                                )
            else:
                res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                  remotepath=configPath,
                                  remotefile=configFileName,
                                  method='download',
                                  repository=data_rev['repositoryInfo']
                                )
            if res == False:
                return 'Fail', "Can't download from %s:s" % (data_rev['repositoryInfo'], configPath)

            i += 1                
            
        stop_res, reason = emStopVnfNone()
        if stop_res == 'Fail':
            return 'Fail', reason
            
        start_res, reason = emStartVnfNone()
        if start_res == 'Fail':
            return 'Fail', reason
            
        result = 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        result = 'Fail', e
    
    return result
    
def getScenario(data, type='scp'):
    log_rev = "****************************************\n"
    log_rev += "VNF Get Scenario - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    data_rev = {}
    data_rev['id'] = data['id']
    data_rev['sync'] = data['sync']
    data_rev['folder'] = []
    data_rev['repositoryInfo'] = {}
    data_rev['repositoryInfo']['type'] = type
    for parameter in data['parameters']:
        index = parameter['index']
        check_folder = False
        for item in parameter['varargs']:
            if item['name'] == 'serverIP':
                data_rev['repositoryInfo']['host'] = item['value']
            elif item['name'] == 'loginID':
                data_rev['repositoryInfo']['user'] = item['value']
            elif item['name'] == 'loginPW':
                data_rev['repositoryInfo']['passwd'] = item['value']
            elif item['name'] == 'Folder':
                check_folder = True
                configFolderPath = item['value']
                
        if check_folder is True:
            for folder in configFolderPath.split(','):
                data_rev['folder'].append(configFolderPath)
    
    try:
        emSetting.emlog.saveLog(getLog('VNF Get Scenario.........'))
        if len(emSetting.DEFAULT_SAVE_DIR) == 0:
            return 'Fail', "Can't find default config folder"

        i = 0
        for remotepath in data_rev['folder']:
            localpath = emSetting.DEFAULT_SAVE_DIR[i]
            emSetting.emlog.saveLog(getLog("Download all from (%s - %s)" % (data_rev['repositoryInfo']['host'], remotepath), True))
            res = emGetPutVnf(localpath=localpath,
                              remotepath=remotepath,
                              method='downloadall',
                              full_repository=data_rev['repositoryInfo']
                            )
            if res == False:
                return 'Fail', "Can't download from %s-%s" % (data_rev['repositoryInfo']['host'], remotepath)

            i += 1
                
        stop_res, reason = emStopVnfNone()
        if stop_res == 'Fail':
            return 'Fail', reason
            
        start_res, reason = emStartVnfNone()
        if start_res == 'Fail':
            return 'Fail', reason

        result = 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        result = 'Fail', e
    
    return result

