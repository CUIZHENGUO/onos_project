import time
import json
import random
import string
import os, sys
import shutil
import psutil
from subprocess import check_output
from threading import Thread, Event
from subprocess import Popen, PIPE
from datetime import datetime
from em2vnmf import emSetting, emNotify
from util.emLog import getLog
from util.emFTPClient import FTPClient
from util.emSCPClient import SCPClient
from util.emGetPut import emGetPutVnf
from util.emCopy import emCopyVnf
from util.emVnf import emCheckVnf
from vnfm2em.emStop import emStopVnfNone
from onosEmFunction import onosFunction
if 'win32' in sys.platform.lower():
    def checkProcessVnf(procname, event):
        def check_proc_psutil(procname):
            isrun = False
            for p in psutil.process_iter():
                try:
                    if p.name().lower() == procname:
                        isrun = True
                        break
                except:
                    pass
            return isrun
            
        procname = procname.lower()      
        time.sleep(3)
        curent_status = True
        while(not event.wait(0.1)):
            isrun = check_proc_psutil(procname)
            if isrun == False and curent_status == True and emSetting.vnf_is_running == True:
                data = {'status': 'Stop', 'reason': '%s is stopped !' % procname}
                emNotify.emNotifyVnf(data)
                curent_status = False
            elif isrun == True and curent_status == False and emSetting.vnf_is_running == True:
                data = {'status': 'Start', 'reason': '%s is started !' % procname}
                emNotify.emNotifyVnf(data)
                curent_status = True
            elif isrun == False and curent_status == False and emSetting.vnf_is_running == False:
                break
            
        """    
        process.wait()
        if emSetting.vnf_is_running is True:
            emSetting.emlog.saveLog(getLog("TestMaster %s Stopped !") % emSetting.config['vnf_name'])
            data = {'status': 'Stop', 'reason': 'TestMaster %s Stopped !' % emSetting.config['vnf_name']}
            emSetting.vnf_is_running = False
            emNotify.emNotifyVnf(data)
        """
else:
    def checkProcessVnf(procname, event):
        def check_proc_subprocess(procname):
            return onosFunction.checkONOS()
            
        procname = procname.lower()
        PROCNAME = procname.upper()
        time.sleep(3)
        curent_status = True
        while (not event.wait(0.1)):
            isrun = check_proc_subprocess(procname)

            if not isrun and procname == "procmgrd" and emSetting.vnf_is_running:
                emSetting.vnf_is_running = False
                event.set()
                data = {'status': 'Stop', 'reason': 'VNF %s Stopped !' % emSetting.config['vnf_name']}
                emNotify.emNotifyVnf(data)
                break
  
            if not isrun and curent_status and emSetting.vnf_is_running:
                data = {'status': 'Stop', 'reason': 'Component [%s] is die.' % PROCNAME}
                emNotify.emNotifyVnf(data)
                #curent_status = False
            elif not curent_status and isrun and emSetting.vnf_is_running:
                data = {'status': 'Stop', 'reason': 'Component [%s] is running.' % PROCNAME}
                emNotify.emNotifyVnf(data)
                #curent_status = True
            elif not isrun and not curent_status and not emSetting.vnf_is_running:
                break

            curent_status = isrun
            if isrun:
                print "curent_status is true "
            else:
                print "curent_status is false "


def removeDirConfig(path):
    for root, dirs, files in os.walk(path):
        try:
            for f in files:
                os.unlink(os.path.join(root, f))
            for d in dirs:
                shutil.rmtree(os.path.join(root, d))
        except Exception as e:
            print e
            pass

def restoreBackup():
    for src_full_path in emSetting.backupfile:
        dest_full_path = src_full_path.split('.backup')[0]
        emCopyVnf(src_path=src_full_path, dest_path=dest_full_path, remove=True)
    emSetting.backupfile[:] = []
            
def emStartVnf(data):
    try:
        if emSetting.vnf_is_running is True:
            emSetting.start_count += 1
            if emSetting.start_count >= int(emSetting.config['max_start_count']):
                emSetting.start_count = 0
                emStopVnfNone()
                emStartVnf(data)
            else:
                return 'Fail', 'vnf (%s, %s) is already running [%s]' % (emSetting.config['vnf_name'], data['tid'], emSetting.start_count)
        if data['deflag'] == 'WITHOUT_CONF':
            log_rev = "****************************************\n"
            log_rev += "VNF Start - Msg rev from VNFM (No config)\n"
            log_rev += json.dumps(data, indent=2)
            log_rev += "\n****************************************"
        elif data['deflag'] == 'WITH_CONF':
            log_rev = "****************************************\n"
            log_rev += "VNF Start - Msg rev from VNFM (With config)\n"
            log_rev += json.dumps(data, indent=2)
            log_rev += "\n****************************************"
        emSetting.emlog.saveLog(getLog(log_rev, True))
        
        if data['deflag'] == 'WITH_CONF':
            numOfconfFile = str(data['numOfconfFile'])
            configpathFile = data['configPathFile'] # list of config path and file
            repositoryInfo = data['repositoryInfo']
            for i in range(0, min(len(emSetting.DEFAULT_SAVE_DIR),int(numOfconfFile))):
                configPath = configpathFile[i]['configPath']
                configFileName = configpathFile[i]['configFileName']
                if configFileName == '*':
                    if len(emSetting.DEFAULT_SAVE_DIR) > 0:
                        localpath = emSetting.DEFAULT_SAVE_DIR[i]
                        #emSetting.LOAD_SCENARIO_PATH = localpath
                        emSetting.emlog.saveLog(getLog("Download all file from %s:%s" % (repositoryInfo, configPath)))
                        res = emGetPutVnf(localpath=localpath,
                              remotepath=configPath,
                              method='downloadall',
                              repository=repositoryInfo
                            )
                        if res is False:
                            return  'Fail', 'Can not download all file'
                    continue

                res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                  remotepath=configPath,
                                  remotefile=configFileName,
                                  method='download',
                                  repository=repositoryInfo
                                )
                if res is False:
                    return 'Fail', 'no such file (%s)' % configFileName
                emSetting.emlog.saveLog(getLog("Deault configfile files: %s" % ','.join(emSetting.defaultConfigFile)))
                if len(emSetting.defaultConfigFile) > i:
                    defaultName = emSetting.defaultConfigFile[i]
                    if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],defaultName)):
                        emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  src_file=defaultName,
                                  dest_file=defaultName+'.backup'
                                )
                        emSetting.backupfile.append(os.path.join(emSetting.DEFAULT_SAVE_DIR[i], defaultName+'.backup'))
                    emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                              dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                              src_file=configFileName,
                              dest_file=defaultName,
                              remove=True
                            )       

        emSetting.emlog.saveLog(getLog('Start VNF.........'))
        try:
            print "Start  EM!"
            emSetting.emlog.saveLog(getLog("Run command: %s" % emSetting.config['start_cmd']))
            #p = Popen(emSetting.config['start_cmd'], shell=True, stdin=PIPE, env=os.environ)
            onosFunction.startONOS()
        except OSError as e:
            restoreBackup()
            emSetting.emlog.saveLog(getLog(e))
            return 'Fail', 'Do not start VNF (%s, %s), pleasr check system' % (emSetting.config['vnf_name'], data['tid']) 
        emSetting.vnf_is_running = True
        emSetting.emlog.saveLog(getLog("Start VNF sucssess"))
        emSetting.event = Event()
        monitor_process = [process.strip() for process in emSetting.config['target_bin'].split('|')]
        #if int(emSetting.config['hb_tcp_port']) == 0:
        for process_name in monitor_process:
            func_thread = Thread(target=checkProcessVnf, args=(process_name, emSetting.event,))
            func_thread.setDaemon(True)
            func_thread.start()
        if 'win32' not in sys.platform.lower() and int(emSetting.config['hb_tcp_port']) > 0:
            emCheckVnf(emSetting.event)
        return 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e
    
def emStartVnfNone():
    try:
        emSetting.emlog.saveLog(getLog('Start VNF.........'))
        print "Start  EM!"
        try:
            emSetting.emlog.saveLog(getLog("Run command: %s" % emSetting.config['start_cmd']))
            #p = Popen(emSetting.config['start_cmd'], shell=True, stdin=PIPE, env=os.environ)
            onosFunction.startONOS()
        except OSError as e:
            emSetting.emlog.saveLog(getLog(e))
            return 'Fail', 'Do not start VNF (%s, %s), pleasr check system' % (emSetting.config['vnf_name'], data['tid']) 
        emSetting.vnf_is_running = True
        emSetting.emlog.saveLog(getLog("Start VNF sucssess"))
        emSetting.event = Event()
        monitor_process = [process.strip() for process in emSetting.config['target_bin'].split('|')]
        #if int(emSetting.config['hb_tcp_port']) == 0:
        for process_name in monitor_process:
            func_thread = Thread(target=checkProcessVnf, args=(process_name, emSetting.event,))
            func_thread.setDaemon(True)
            func_thread.start()
        if 'win32' not in sys.platform.lower() and int(emSetting.config['hb_tcp_port']) > 0:
            emCheckVnf(emSetting.event)
            
        return 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e

