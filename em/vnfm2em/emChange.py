import json
import os
from datetime import datetime
from vnfm2em.emStop import emStopVnfNone
from vnfm2em.emStart import emStartVnfNone
from em2vnmf import emSetting
from util.emLog import getLog
from util.emGetPut import emGetPutVnf
from util.emCopy import emCopyVnf


def emChangeVnf(data):
    keyvallst = data['keyvallst']
    numOfkeys = data['numOfkeys']
    log_rev = "****************************************\n"
    log_rev += "VNF Config Change - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    try:
        emSetting.emlog.saveLog(getLog('Change VNF Config.........'))
        keyvallst = data['keyvallst']
        numOfkeys = data['numOfkeys']
        key0_index = next(index for (index, d) in enumerate(keyvallst) if d["Key"].lower() == "key0")
        print key0_index
        action = int(keyvallst[key0_index]['Val'])
        print action
        del keyvallst[key0_index]
        for i in range(0, int(numOfkeys)-1):
            Key = keyvallst[i]['Key'].lower()
            Val = keyvallst[i]['Val']
            if action == 1:
                if Key == u'key1': repositoryInfo = Val
                elif Key == u'key2': numOfconfFile = Val
                elif Key == u'key3': configPathFile = Val
            elif action == 2:
                if Key == u'key1': repositoryInfo = Val
                elif Key == u'key2': numOfconfFile = Val
                elif Key == u'key3': configPathFile = Val
                elif Key == u'key4': saveFlag = int(Val)
            elif action == 3:
                if Key == u'key1': logFilePath = Val
                elif Key == u'key2': logFileName = Val
            elif action == 4:
                if Key == u'key1': status = Val
                elif Key == u'key2': reason = Val
        if action == 1:
            for i in range(0, int(numOfconfFile)):
                configPath = configPathFile[i]['configPath']
                configFileName = configPathFile[i]['configFileName']
                res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                  remotepath=configPath,
                                  remotefile=configFileName,
                                  method='download',
                                  repository=repositoryInfo,
                                  backup=True
                                )
                if res is False:
                    return 'Fail', 'no such file (%)' % configFileName
            emStopVnfNone()
            emStartVnfNone()
        elif action == 2:
            if saveFlag == 0:
                for i in range(0, int(numOfconfFile)):
                    configPath = configPathFile[i]['configPath']
                    configFileName = configPathFile[i]['configFileName']
                    res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                      remotepath=configPath,
                                      remotefile=configFileName,
                                      method='download',
                                      repository=repositoryInfo,
                                      backup=True
                                    )
                    if res is False:
                        return  'Fail', 'no such file (%)' % configFileName
                emStopVnfNone()
                emStartVnfNone()
            elif saveFlag == 1:
                for i in range(0, int(numOfconfFile)):
                    configPath = configPathFile[i]['configPath']
                    configFileName = configPathFile[i]['configFileName']
                    
                    if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],configFileName)):
                        # Backup
                        res = emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  src_file=configFileName,
                                  dest_file=configFileName+'.backup',
                                  remove=False
                                )
                        # Rename
                        #newfile, file_extension = os.path.splitext(configFileName)
                        #str_day = datetime.utcnow().strftime("_%Y%m%d_%H%M%S")
                        #newfile = newfile + str_day + file_extension
                        #if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],newfile)):
                        #    os.remove(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],newfile))
                        #res = emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                        #                dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                        #                src_file=configFileName,
                        #                dest_file=newfile
                        #        )
                        # Upload
                        res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                          remotepath=configPath,
                                          localfile=configFileName,
                                          method='upload',
                                          repository=repositoryInfo
                                    )
                        if res is False:
                             return  'Fail', 'Can not upload file (%)' % configFileName
                        os.remove(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],configFileName))
                        # Download
                        res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                          remotepath=configPath,
                                          remotefile=configFileName,
                                          method='download',
                                          repository=repositoryInfo,
                                          backup=False
                                    )
                        if res is False:
                            return  'Fail', 'no such file (%)' % configFileName
                    else:
                         return  'Fail', 'no such file (%)' % configFileName
                emStopVnfNone()
                emStartVnfNone()
        elif action == 3:
            print 'action 3 not yet'
        elif action == 4:
            print 'action 4 not yet'
        result = 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        result = 'Fail', e
    
    return result 
