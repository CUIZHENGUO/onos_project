import json
import os
import sys
import shutil
from datetime import datetime
from subprocess import Popen, PIPE
from em2vnmf import emSetting
from util.emLog import getLog
from util.emCopy import emCopyVnf
from util.emGetPut import emGetPutVnf
from onosEmFunction import onosFunction

def emStopVnf(data, restore=True, backup=True):
    if emSetting.vnf_is_running is False:
        return 'Fail', 'VNF (%s, %s) was stopped' % (emSetting.config['vnf_name'], data['tid'])
    if data['saveflag'] == 'WITHOUT_SAVE':
        log_rev = "****************************************\n"
        log_rev += "VNF Stop - Msg rev from VNFM (No Save)\n"
        log_rev += json.dumps(data, indent=2)
        log_rev += "\n****************************************"
    elif  data['saveflag'] == 'WITH_SAVE':
        numOfconfFile = data['numOfconfFile']
        repositoryInfo = data['repositoryInfo']
        configpathFile = data['configPathFile']

        log_rev = "****************************************\n"
        log_rev += "VNF Stop - Msg rev from VNFM (With Save)\n"
        log_rev += json.dumps(data, indent=2)
        log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    try:
        emSetting.emlog.saveLog(getLog('Stop VNF.........'))
        if data['saveflag'] == 'WITH_SAVE':
            numOfconfFile = data['numOfconfFile']
            repositoryInfo = data['repositoryInfo']
            configpathFile = data['configPathFile']
            for i in range(0, min(len(emSetting.DEFAULT_SAVE_DIR),int(numOfconfFile))):
                remotepath = configpathFile[i]['configPath']
                filename = configpathFile[i]['configFileName']

                if filename == '*':
                    if len(emSetting.LOAD_SCENARIO_PATH) > 0:
                        emSetting.emlog.saveLog(getLog("Upload %s to %s" % (emSetting.LOAD_SCENARIO_PATH[i], repositoryInfo)))
                        res = emGetPutVnf(localpath=emSetting.LOAD_SCENARIO_PATH[i],
                                          remotepath=remotepath,
                                          method='uploadall',
                                          repository=repositoryInfo
                                        )
                        if res is False:
                            return  'Fail', 'Can not upload all file'

                    continue

                if backup and len(emSetting.defaultConfigFile) > i:
                    defaultName = emSetting.defaultConfigFile[i]
                    emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                              dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                              src_file=defaultName,
                              dest_file=filename,
                              remove=False
                            )		
                    res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                      remotepath=remotepath,
                                      localfile=filename,
                                      method='upload',
                                      repository=repositoryInfo
                                   )
                    if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i], filename)):
                        os.remove(os.path.join(emSetting.DEFAULT_SAVE_DIR[i], filename))
        try:
            emSetting.emlog.saveLog(getLog("Run command: %s" % emSetting.config['stop_cmd']))
            print "Stop  EM!"
            emSetting.vnf_is_running = False
            #p = Popen(emSetting.config['stop_cmd'], shell=True)
            #p.wait()
            onosFunction.stopONOS(True)

        except OSError as e:
            emSetting.emlog.saveLog(getLog(e))
            return 'Fail', 'Do not stop VNF (%s, %s), please check system.' % (emSetting.config['vnf_name'], data['tid'])
        
        if len(emSetting.DEFAULT_SAVE_DIR) > 0 and 'win32' in sys.platform.lower():
            for i in range(0, len(emSetting.DEFAULT_SAVE_DIR)):
                for root, dirs, files in os.walk(emSetting.DEFAULT_SAVE_DIR[i]):
                    try:
                        for f in files:
                            os.unlink(os.path.join(root, f))
                        for d in dirs:
                            shutil.rmtree(os.path.join(root, d))
                    except:
                        pass
            emSetting.LOAD_SCENARIO_PATH = emSetting.DEFAULT_SAVE_DIR
            
        if restore:
            for src_full_path in emSetting.backupfile:
                dest_full_path = src_full_path.split('.backup')[0]
                emCopyVnf(src_path=src_full_path, dest_path=dest_full_path, remove=True)
            emSetting.backupfile[:] = []
            
            for i in range(0, len(emSetting.DEFAULT_SAVE_DIR)):
                for defaultfilename in emSetting.defaultConfigFile:
                    if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],defaultfilename+'.backup')):
                        if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],defaultfilename)):
                            os.remove(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],defaultfilename))
                        emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                                  src_file=defaultfilename+'.backup',
                                  dest_file=defaultfilename,
                                  remove=True
                            )
        emSetting.start_count = 0
        if emSetting.event:
            emSetting.event.set()
        emSetting.emlog.saveLog(getLog("Stop VNF Success"))
        return 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e
    
def emStopVnfNone():
    try:
        try:
            emSetting.emlog.saveLog(getLog('Stop VNF.........'))
            emSetting.emlog.saveLog(getLog("Run command: %s" % emSetting.config['stop_cmd']))
            print "Stop  EM!"
            emSetting.vnf_is_running = False
            # p = Popen(emSetting.config['stop_cmd'], shell=True)
            # p.wait()
            onosFunction.stopONOS(True)
        except OSError as e:
            emSetting.emlog.saveLog(getLog(e))
            return 'Fail', 'Do not stop VNF (%s, %s), please check system.' % (emSetting.config['vnf_name'], data['tid'])
        
        emSetting.start_count = 0
        if emSetting.event:
            emSetting.event.set()
        emSetting.emlog.saveLog(getLog("Stop VNF Success"))
        return 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e

