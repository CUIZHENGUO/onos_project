#!/usr/bin/python

"""
Description: ONOS Functions for ONOS EM.
Date: 2017/01/02
Author: zhenguo cui
contact: quejinkook@korea.ac.kr

Function List:
- onosCluster(data): set ONOS Cluster by input data
- onosAppManage(data): manage ONOS application by input data
- onosAppCheck(data): check if onos is activate or not
"""
import json
import os
from em2vnmf import emSetting
from util.emLog import getLog
from onosEmFunction import onosFunction


def onosCluster(data):
    onos_ip = data['ip']
    ip_list = data['ipList']
    log_rev = "****************************************\n"
    log_rev += "ONOS Cluster - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))

    try:
        result = onosFunction.setClusterInfo(onos_ip,ip_list,True)
        return 'Success', ''
    except Exception as e:
        onosFunction.logMessage("onosCluster", getLog(e))
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e

def onosAppManage(data):
    app_name = data['appName']
    activate = data['activate']
    log_rev = "****************************************\n"
    log_rev += "ONOS App Manager - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    onosFunction.logMessage("onosAppManage: log receive", log_rev)
    onosFunction.logMessage("onosAppManage: activate data", activate)
    if activate == "True":
        activate = True
        onosFunction.logMessage("onosAppManage", "True")
    else:
        activate = False
        onosFunction.logMessage("onosAppManage", "False")

    try:
        result = onosFunction.manageONOSApp(app_name, activate)
        return 'Success', ''
    except Exception as e:
        onosFunction.logMessage("onosAppManage", getLog(e))
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e

def onosAppCheck(data):
    app_name = data['appName']

    log_rev = "****************************************\n"
    log_rev += "ONOS App Checker - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))

    try:
        result = onosFunction.checkONOSApp(app_name)
        onosFunction.logMessage("onosAppCheck: result", result)
        # return activate or deactivate
        if result == "ACTIVE":
            return 'Success', 'Activate'
        else:
            return 'Success', 'DeActivate'
    except Exception as e:
        onosFunction.logMessage("onosAppCheck", getLog(e))
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e
