import json
import os
from datetime import datetime
from em2vnmf import emSetting
from util.emLog import getLog
from util.emCopy import emCopyVnf
from util.emGetPut import emGetPutVnf


def emSaveVnf(data):
    log_rev = "****************************************\n"
    log_rev += "VNF Config Save - Msg rev from VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    try:
        emSetting.emlog.saveLog(getLog('Save VNF Config.........'))
        numOfconfFile = data['numOfconfFile']
        repositoryInfo = data['repositoryInfo']
        configPathFile = data['configPathFile']
        for i in range(0, min(len(emSetting.DEFAULT_SAVE_DIR),int(numOfconfFile))):
            remotepath =  configPathFile[i]['configPath']
            filename = configPathFile[i]['configFileName']
            if filename == '*':
                if len(emSetting.LOAD_SCENARIO_PATH) > 0:
                    emSetting.emlog.saveLog(getLog("Upload %s to %s" % (emSetting.LOAD_SCENARIO_PATH[i], repositoryInfo)))
                    res = emGetPutVnf(localpath=emSetting.LOAD_SCENARIO_PATH[i],
                                      remotepath=remotepath,
                                      method='uploadall',
                                      repository=repositoryInfo
                                    )
                    if res is False:
                        return  'Fail', 'Can not upload (%s)' % emSetting.LOAD_SCENARIO_PATH[i]
            elif len(emSetting.defaultConfigFile) > i:
                defaultName = emSetting.defaultConfigFile[i]
                # Copy defaut config name to filename and upload to server
                if os.path.exists(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],defaultName)):
                    emCopyVnf(src_path=emSetting.DEFAULT_SAVE_DIR[i],
                              dest_path=emSetting.DEFAULT_SAVE_DIR[i],
                              src_file=defaultName,
                              dest_file=filename
                            )
                    res = emGetPutVnf(localpath=emSetting.DEFAULT_SAVE_DIR[i],
                                      remotepath=remotepath,
                                      localfile=filename,
                                      method='upload',
                                      repository=repositoryInfo
                                   )
                    os.remove(os.path.join(emSetting.DEFAULT_SAVE_DIR[i],filename))
                    if res is False:
                        return  'Fail', 'Can not upload file (%s)' % filename
                else:
                    return  'Fail', 'No such default config file (%s)' % filename
            else:
                return  'Fail', 'No default config file found from VNF'
                      
        result = 'Success', ''
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        result = 'Fail', e
    
    return result
