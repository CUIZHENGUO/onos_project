#!/usr/bin/env python
# -*- coding: utf8 -*-

import sys
import socket
import threading
import time
import struct
import binascii
import json

from em2vnmf.emNotify import emNotifyVnf
from em2vnmf import emSetting
from util.emLog import getLog

VNF_STOP    = 0x10
VNF_START   = 0x11
VNF_RESTART = 0x12

class ClientThread(threading.Thread):
    '''
    Class that implements the client threads in this server
    '''

    def __init__(self, client_sock):
        '''
        Initialize the object, save the socket that this thread will use.
        '''

        threading.Thread.__init__( self )
        self.client = client_sock
        
    def heartbit(self):
        pass

    def run( self ):
        '''
        Thread's main loop. Once this function returns, the thread is finished
        and dies.
        '''

        cmd = self.readline()

        # Read data from the socket and process it
        try:
            while True:
                if len(cmd) > 45: # msg_type = 1 byte, msg_len = 1 bytes, min(msg)=41 <'{'status':'stop','reason':''}'> -> min_len is 45
                    msg_in_hex = binascii.hexlify(cmd)
                    if int(msg_in_hex[0:2], 16) == VNF_STOP:
                        msg_len = int(msg_in_hex[3:10], 16)
                        s = struct.Struct('! B' + ' I ' + str(msg_len) + 's')
                        unpkt_data = s.unpack(cmd)
                        data = unpkt_data[2]
                        emSetting.emlog.saveLog(getLog("New msg notify: %s" % data))
                        emNotifyVnf(json.loads(data))
                        self.writeline("OK")
                    elif int(msg_in_hex[0:2], 16) == VNF_START:
                        pass
                    elif int(msg_in_hex[0:2], 16) == VNF_RESTART:
                        pass
                elif cmd == '' or cmd == None:
                    break
                cmd = self.readline()
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
        finally:
            # Make sure the socket is closed once we're done with it
            self.client.close()

        return

    def readline(self):
        '''
        Helper function, reads up to 20148 chars from the socket, and returns
        them as a string, and without any end of line markers
        '''
        
        result = ''
        try:
            result = self.client.recv(2048)
        except Exception as e:
            raise e
        if(None != result):
            result = result.strip()
        return result

    def writeline(self, text):
        '''
        Helper function, writes teh given string to the socket, with an end of
        line marker appended at the end
        '''

        self.client.send(text.strip() + '\n')

class TCPServer():
    '''
    Server class. Opens up a socket and listens for incoming connections.
    Every time a new connection arrives, it creates a new ClientThread thread
    object and defers the processing of the connection to it.
    '''

    def __init__(self, host='0.0.0.0', port=1129):
        self.sock = None
        self.thread_list = []
        self.host = host
        self.port = port

    def run( self ):
        '''
        Server main loop.
        Creates the server (incoming) socket, and listens on it of incoming
        connections. Once an incomming connection is deteceted, creates a
        ClientThread to handle it, and goes back to listening mode.
        '''
        all_good = False
        try_count = 0
        # Attempt to open the socket
        while not all_good:
            if 5 < try_count:
                # Tried more than 5 times, without success... Maybe the port
                # is in use by another program
                sys.exit(1)
            try:
                # Create the socket
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # Bind it to the interface and port we want to listen on
                self.sock.bind((self.host, self.port))

                # Listen for incoming connections. This server can handle up to
                # 50 simultaneous connections
                self.sock.listen(50)
                all_good = True
                break
            except socket.error, err:
                # Could not bind on the interface and port, wait for 5 seconds
                # print 'Socket connection error... Waiting 5 seconds to retry.'
                del self.sock
                time.sleep(5)
                try_count += 1
                
        try:
            while True:
                try:
                    # Wait for 3 seconds for incoming connections
                    self.sock.settimeout(3)
                    client = self.sock.accept()[0]
                except socket.timeout:
                    # No connection detected, continue
                    continue
                    
                # Create the ClientThread object and let it handle the incoming
                # connection
                new_thread = ClientThread(client)
                emSetting.emlog.saveLog(getLog("New client connect: %s" % client.getpeername()[0]))
                self.thread_list.append( new_thread )
                new_thread.start()

                # Go over the list of threads, remove those that have finished
                # (their run method has finished running) and wait for them
                # to fully finish
                for thread in self.thread_list:
                    if not thread.isAlive():
                        self.thread_list.remove(thread)
                        thread.join()

        #except KeyboardInterrupt:
        #    print 'Ctrl+C pressed... Shutting Down'
        except Exception, err:
            emSetting.emlog.saveLog(getLog('Exception caught: %s\nClosing...' % err))

        # Clear the list of threads, giving each thread 1 second to finish
        # NOTE: There is no guarantee that the thread has finished in the
        #    given time. You should always check if the thread isAlive() after
        #    calling join() with a timeout paramenter to detect if the thread
        #    did finish in the requested time
        for thread in self.thread_list:
            thread.join(1.0)

        # Close the socket once we're done with it
        self.sock.close()

if "__main__" == __name__:
    try:
        server = TCPServer()
        server.run()
    except Exception as e:
        print e
        exit(0)