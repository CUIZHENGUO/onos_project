import json
import struct
import binascii
import socket
import time
import sys
import threading
import psutil
import datetime
from em2vnmf import emSetting
from util.emLog import getLog
from em2vnmf import emNotify

# define Type EM
CMD_EM_DEREG = 0x10 # notifi COMPONET disconnect
CMD_EM_OK = 0x11  # connec CM OK
CMD_EM_REGIS = 0x12  # send commponet connected

# request
EM_REQUEST_CM = 0x13 # requset check CM
EM_REQUEST_COM = 0x14 # request check component conected
MD_EM_UNKOW = 0x15	# not type
      
def checkStatusVnf():
    result = 'Start' if emSetting.vnf_is_running == True else 'Stop'
    return result
        
def createPacket(emType, emMsg):
    emLen = len(emMsg)
    values = (emType, emLen, emMsg)
    s = struct.Struct('! I' + ' I ' + str(emLen) + 's') 
    return s.pack(*values)
          
class TCPClient(threading.Thread):
    def __init__(self, event, host='localhost', port=0):
        threading.Thread.__init__(self )
        self.host = host
        self.port = port
        self.event = event

    def connect(self):
        try_count = 0
        while(True):
            if 2 < try_count:
                # Tried more than 3 times, without success... Maybe the server is die
                sys.exit(1)
            try:
                # Create the socket
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                # connect to server
                self.sock.connect((self.host, self.port))
                emSetting.emlog.saveLog(getLog("Connect (%s:%s) success" % (self.host, self.port)))
                return True
            except socket.error, err:
                # Could not connect to server, wait for 2 seconds
                del self.sock
                time.sleep(2)
                try_count += 1
        return False
        
    def checkVnf(self):
        while(True):
            from_now = datetime.datetime.now()
            if (from_now - self.end_time).total_seconds() > 9:
                emSetting.emlog.saveLog(getLog("VNF %s Stopped !") % emSetting.config['vnf_name'])
                self.event.set()
                emSetting.vnf_is_running = False
                data = {'status': 'Stop', 'reason': 'VNF %s Stopped !' % emSetting.config['vnf_name']}
                emNotify.emNotifyVnf(data)
                break
            time.sleep(3)

    def run(self):
        is_conn = self.connect()
        self.sock.settimeout(1)
        if is_conn is True:
            self.end_time = datetime.datetime.now()
            msg_send = createPacket(EM_REQUEST_CM, ".")
            count = 0
            while(not self.event.wait(3)):
                resulr = self.writeline(msg_send)
                
                if resulr is False:
                    count += 1
                    emSetting.emlog.saveLog(getLog("Send Heartbeat message to VNF: Fail[%d]" % count))
                    try:
                        # Create the socket
                        del self.sock
                        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                        # connect to server
                        self.sock.connect((self.host, self.port))
                        self.sock.settimeout(1)
                        #self.writeline(msg_send)
                    except Exception as e:
                       emSetting.emlog.saveLog(getLog("Reconnect err (%s)" % e))
                       #pass	
                    if count >= 3:
                        emSetting.emlog.saveLog(getLog("VNF %s Stopped !") % emSetting.config['vnf_name'])
                        emSetting.vnf_is_running = False
                        data = {'status': 'Stop', 'reason': 'VNF %s Stopped !' % emSetting.config['vnf_name']}
                        emNotify.emNotifyVnf(data)
                        self.event.set()
                        break
                #print 'begin read msg'
                msg_rev = self.readline()
                #print 'end read msg'
                len_msg = len(msg_rev)
                #print "len(msg)=", len_msg
                if len_msg > 0:
                    l = 0
                    try:
                        while(l < len_msg):
                            msg_in_hex = binascii.hexlify(msg_rev[l:])
                            #print msg_in_hex
                            istype = socket.htonl(int(msg_in_hex[0:8], 16))
                            if istype == CMD_EM_OK:
                                islen = socket.htonl(int(msg_in_hex[8:16], 16))
                                emSetting.emlog.saveLog(getLog("Send Heartbeat message to VNF: Success"))
                                #self.end_time = datetime.datetime.now()
                                count = 0
                            elif istype == CMD_EM_DEREG:
                                islen = socket.htonl(int(msg_in_hex[8:16], 16))
                                msg_data = msg_rev[l+8:l+8+islen]
                                #print msg_data
                                emSetting.emlog.saveLog(getLog("COMPONET is disconnect: %s" % msg_data))
                                data = {'status': 'Stop', 'reason': msg_data.strip()}
                                emNotify.emNotifyVnf(data)
                            elif istype == CMD_EM_REGIS:
                                islen = socket.htonl(int(msg_in_hex[8:16], 16))
                                msg_data = msg_rev[l+8:l+8+islen]
                                #print msg_data
                                emSetting.emlog.saveLog(getLog("COMPONET is connect: %s" % msg_data))
                                data = {'status': 'Start', 'reason': msg_data.strip()}
                                emNotify.emNotifyVnf(data)
                            l += 8 + islen # 4+4+1024
                    except Exception as e:
                        print 'err:', e
                    msg_rev = ''
                else:
                    pass
                
            emSetting.emlog.saveLog(getLog("Stop Heartbeat message to VNF"))

    def readline(self):
        '''
        Helper function, reads up to 20148 chars from the socket, and returns
        them as a string, and without any end of line markers
        '''
        
        result = ''
        try:
            result = self.sock.recv(10320)
        except Exception as e:
            pass
        if(None != result):
            result = result.strip()
        return result

    def writeline(self, msg):
        '''
        Helper function, writes teh given string to the socket
        '''
        try:
            self.sock.send(msg.strip() + '\n')
            return True
        except:
            return False
        
def emCheckVnf(event):
    host = emSetting.config['em_ip']
    port = int(emSetting.config['hb_tcp_port'])
    tcp_client = TCPClient(event, host, port)
    tcp_client.setDaemon(True)
    tcp_client.start()
