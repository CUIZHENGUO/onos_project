import json
import inspect
import logging
import os
from os import listdir
from os.path import basename, splitext, isfile, join, exists
from logging.handlers import RotatingFileHandler
from datetime import datetime


def getLog(log=None, parse=False):
    callerframerecord = inspect.stack()[1]    # 0- represents this line, 1- represents line at caller
    frame = callerframerecord[0]
    info = inspect.getframeinfo(frame)
    __FILE__ = basename(info.filename)
    __LINE__ = info.lineno
    date_str = datetime.utcnow().strftime("[%Y%m%d %H:%M:%S]")
    if log is None:
        return date_str + '[em:%s:%s]' % (__FILE__, __LINE__)
    elif parse is False:
        return date_str + '[em:%s:%s]' % (__FILE__, __LINE__) + '[%s]'%log
    else:
        return date_str + '[em:%s:%s]' % (__FILE__, __LINE__) + ', Info:\n' + log


class EMRotatingFileHandler(RotatingFileHandler):
    def __init__(self, parent, filename, mode='a', maxBytes=0, backupCount=0, encoding=None, delay=0):
        self.parent = parent
        super(EMRotatingFileHandler, self).__init__(filename, mode, maxBytes, backupCount, encoding, delay)

    def doRollover(self):
        if self.stream:
            self.stream.close()
            self.stream = None
 
        netxt_file = self.parent.getNextLogFileName()
        if os.path.exists(netxt_file):
            os.remove(netxt_file)
        self.parent.newLogFile(netxt_file)
        if not self.delay:
            self.stream = self._open()

class EMLogger(object):
    def __init__(self, log_dir='./log', vnf_name='l3'):
        self.log_dir = log_dir
        self.vnf_name = vnf_name
        self.logfile = self.getLogFileName()
        self.formatter = logging.Formatter('%(message)s')
        self.islogfile = False
        self.isconsole = False
        self.initialize()

    def getLogFileName(self):
        onlyfiles = sorted([f for f in listdir(self.log_dir) if (isfile(join(self.log_dir, f)) and self.vnf_name in f)])
        date_str = datetime.utcnow().strftime("%Y%m%d")
        if len(onlyfiles) == 0:
            logfile = 'em_' + self.vnf_name + '_' + date_str + "_001.log"
        elif date_str in onlyfiles[-1]:
            logfile = onlyfiles[-1]
        else:
            filename, file_extension = splitext(onlyfiles[-1])
            num = filename[::-1].split('_')[0]
            num = int(num[::-1])
            logfile = 'em_' + self.vnf_name + '_' + date_str + '_' + '%03d'%num + '.log'
        return logfile

    def setLogFileName(self, filename):
        self.logfile = filename

    def getNextLogFileName(self):
        date_str = datetime.utcnow().strftime("%Y%m%d")
        filename, file_extension = splitext(self.logfile)
        num = filename[::-1].split('_')[0]
        num = int(num[::-1]) + 1
        if num == 1000:
            num = 1
        logfile = 'em_' + self.vnf_name + '_' + date_str + '_' + '%03d'%num + '.log'
        return logfile

    def newLogFile(self, filename=None):
        if filename is None:
            filename = self.getNextLogFileName()
        self.setLogFileName(filename)
        self.initialize(first=False)

    def initialize(self, first=True):
        if first == False:
            for hdlr in self.log.handlers[:]:
                self.log.removeHandler(hdlr)
        self.handler = EMRotatingFileHandler(parent=self, filename=self.log_dir+'/'+self.logfile, mode='a', maxBytes=5*1024*1024, backupCount=0, encoding=None, delay=0)
        self.handler.setFormatter(self.formatter)
        self.log = logging.getLogger(__name__)
        self.log.setLevel(logging.INFO)
        self.log.addHandler(self.handler)           

    def saveLog(self, msg):
        if self.islogfile:
            self.log.info(msg)
        if self.isconsole:
            print msg
            
    def startLogFile(self):
        self.islogfile = True
        
    def stopLogFile(self):
        self.islogfile = False
        
    def enableisconsoleLog(self):
        self.isconsole = True
        
    def disableisconsoleLog(self):
        self.isconsole = False
