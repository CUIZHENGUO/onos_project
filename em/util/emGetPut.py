import random
import string
import os
from em2vnmf import emSetting
from util.emLog import getLog
from util.emFTPClient import FTPClient
from util.emSCPClient import SCPClient
from util.emCopy import emCopyVnf

def emGetPutVnf(localpath='', remotepath='/', localfile=None, remotefile=None, method=None,
                repository=None, backup=True, full_repository=None):
    try:
        if method is None:
            return False
            
        if repository:
            repo_data = repository.split('://')
            if len(repo_data) < 2 or (repo_data[0].lower() != 'scp' and repo_data[0].lower() != 'ftp'):
                return False
             
            repo_type = repo_data[0].lower()
            host = repo_data[1].replace('/', '')
            if repo_type == 'ftp':
                user = emSetting.config['ftp_user']
                passwd = emSetting.config['ftp_passwd']
                client = FTPClient(host=host, user=user, passwd=passwd)
            elif repo_type == 'scp':
                user = emSetting.config['scp_user']
                passwd = emSetting.config['scp_passwd']
                client = SCPClient(host=host, user=user, passwd=passwd)
            else:
                return False
                
        elif full_repository:
            repo_type = full_repository['type']
            host = full_repository['host']
            user = full_repository['user']
            passwd = full_repository['passwd']
            if repo_type == 'ftp':
                client = FTPClient(host=host, user=user, passwd=passwd)
            elif repo_type == 'scp':
                client = SCPClient(host=host, user=user, passwd=passwd)
            else:
                return False
        else:
            return False
            
        if method.lower() == "download":
            if remotefile is None:
                return False
            if backup:
                randname = ''.join(random.choice(string.lowercase) for i in range(16))
                res = client.autoDownload(filename=remotefile,
                                          remotepath=remotepath,
                                          localpath=localpath,
                                          changename=randname
                                        )
                if res is True:
                    if os.path.exists(os.path.join(localpath+remotefile)):
                        emCopyVnf(src_path=localpath,
                                  dest_path=localpath,
                                  src_file=remotefile,
                                  dest_file=remotefile+'.backup',
                                  remove=True
                                )
                    emCopyVnf(src_path=localpath,
                              dest_path=localpath,
                              src_file=randname,
                              dest_file=remotefile,
                              remove=True
                            )
                if os.path.exists(os.path.join(localpath,randname)):
                    os.remove(os.path.join(localpath,randname))
            else:
                res = client.autoDownload(filename=remotefile,
                                          remotepath=remotepath,
                                          localpath=localpath
                                        )
            return res
        elif method.lower() == "upload":
            if localfile is None:
                return False
            res = client.autoUpload(filename=localfile,
                                    remotepath=remotepath,
                                    localpath=localpath
                                )
            return res
        elif method.lower() == "downloadall":
            res = client.autoDownloadAll(remotepath=remotepath, localpath=localpath)
            return res
        elif method.lower() == "uploadall":
            res = client.autoUploadAll(localpath=localpath, remotepath=remotepath)
            return res
        else:
            emSetting.emlog.saveLog(getLog("method %s is not suppor" % method))
            return False
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return False
        
    return True
