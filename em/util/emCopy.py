import os
import shutil
#import stat
from em2vnmf import emSetting
from util.emLog import getLog


def emCopyVnf(src_path='./', dest_path='./', src_file='', dest_file='', remove=False):
    src_file = os.path.join(src_path,src_file)
    if not os.path.exists(src_file):
        return False
    dest_file = os.path.join(dest_path,dest_file)
    if os.path.exists(dest_file):
        os.remove(dest_file)
    try:
        shutil.copy2(src_file, dest_file)
        if remove:
            os.remove(src_file)
        return True
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return False
