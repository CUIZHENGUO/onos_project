import os
import shutil
from paramiko import SSHClient, AutoAddPolicy
from stat import S_ISDIR
from em2vnmf import emSetting
from util.emLog import getLog

class SCPClient():
    def __init__(self, host='localhost', user='root', passwd='root'):
        self.bufSize = 4096
        self.host = host
        self.user = user
        self.passwd = passwd
        self.ssh = SSHClient()
        self.ssh.set_missing_host_key_policy(AutoAddPolicy())
        self.connected = False
        self.sftp = None
        
    def __exit__(self, type, value, traceback):
        self.close()
    
    def connect(self):
        try:
            self.ssh.connect(self.host, username=self.user, password=self.passwd)
            self.connected = True
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            self.connected = False
        return self.connected
               
    def get(self, remotepath, localpath):
        try:
            remotepath = remotepath.replace("\\", "/")
            if self.connected is False:
                isconnect = self.connect()
                if not isconnect:
                    return False
            self.sftp = self.ssh.open_sftp()
            self.sftp.get(str(remotepath), str(localpath))
            return True
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            return False
            
    def put(self, localpath, remotepath):
        try:
            remotepath = remotepath.replace("\\", "/")
            if self.connected is False:
                isconnect = self.connect()
                if not isconnect:
                    return False
            self.sftp = self.ssh.open_sftp()    
            self.sftp.put(localpath, remotepath)
            return True
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            return False
            
    def autoUpload(self, filename='', remotepath='/', localpath=''):
        if filename == '':
            return False
        try:
            isconnect = self.connect()
            if not isconnect:
                return False
            res = self.put(os.path.join(localpath,filename), os.path.join(remotepath,filename))
            return res
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            return False
        finally:
            self.close()
        
    def autoDownload(self, filename='', remotepath='/', localpath='', changename=None):
        if filename == '':
            return False
        changename = filename if changename else changename
        try:
            isconnect = self.connect()
            if not isconnect:
                return False
            res = self.get(os.path.join(remotepath,filename), os.path.join(localpath,changename))
            return res
        except Exception as e:
            if os.path.exists(os.path.join(localpath,changename)):
                os.remove(os.path.join(localpath,changename))
            emSetting.emlog.saveLog(getLog(e))
            return False
        finally:
            self.close()
        
    def sftp_walk(self,remotepath):
        # Kindof a stripped down  version of os.walk, implemented for sftp.  Tried running it flat 
        # without the yields, but it really chokes on big directories.
        path = remotepath
        files = []
        folders=[]
        for f in self.sftp.listdir_attr(remotepath):
            if S_ISDIR(f.st_mode):
                folders.append(f.filename)
            else:
                files.append(f.filename)
        yield path, folders, files
        for folder in folders:
            new_path = os.path.join(remotepath, folder).replace("\\", "/")
            for x in self.sftp_walk(new_path):
                yield x

    def autoDownloadAll(self, remotepath='', localpath=''):
        # recursively download a full directory Harder than it sounded at first, since paramiko 
        # won't walk
        #
        # For the record, something like this would gennerally be faster:
        # ssh user@host 'tar -cz /source/folder' | tar -xz
        try:
            isconnect = self.connect()
            if not isconnect:
                return False
                
            if remotepath[-1] != '/':
                remotepath += '/'
                
            self.sftp = self.ssh.open_sftp()
            self.sftp.chdir(remotepath)
            folder_create = []
            listdownload = []
            try:
                os.mkdir(localpath)
            except:
                pass
            for walker in self.sftp_walk('.'):
                try:
                    os.mkdir(os.path.join(localpath,walker[0]))
                    folder_create.append(os.path.join(localpath,walker[0]))
                except:
                    pass
                for file in walker[2]:
                    emSetting.emlog.saveLog(getLog("Download file %s" % file))
                    self.sftp.get(os.path.join(walker[0],file).replace("\\", "/"),os.path.join(localpath,walker[0],file))
                    listdownload.append(os.path.join(localpath,walker[0],file))
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            for folder in folder_create:
                shutil.rmtree(folder)
            for file in listdownload:
                if os.path.exists(file): 
                    os.remove(file)
            return False
        finally:
            self.close()
    
    def mkdir(self, path, mode=511, ignore_existing=False):
        ''' Augments mkdir by adding an option to not fail if the folder exists  '''
        try:
            self.sftp.mkdir(path, mode)
        except IOError:
            if ignore_existing:
                pass
            else:
                raise
    
    def uploadDir(self, source, target):
        for item in os.listdir(source):
            if os.path.isfile(os.path.join(source, item)):
                emSetting.emlog.saveLog(getLog("Upload file %s" % item))
                self.sftp.put(os.path.join(source, item), '%s/%s' % (target, item))
            else:
                self.mkdir('%s/%s' % (target, item), ignore_existing=True)
                self.uploadDir(os.path.join(source, item), '%s/%s' % (target, item))
                
    def autoUploadAll(self, localpath='', remotepath=''):
        try:
            isconnect = self.connect()
            if not isconnect:
                return False
            self.sftp = self.ssh.open_sftp()
            self.mkdir(remotepath, ignore_existing=True)
            self.uploadDir(localpath, remotepath)
        except Exception as e:
            emSetting.emlog.saveLog(getLog(e))
            return False
        finally:
            self.close()
     
    def close(self):
        """close channel"""
        if self.sftp:
            try:
                self.sftp.close()
            except:
                pass
        if self.ssh is not None:
            try:
                self.ssh.close()
            except:
                pass
        self.connected = False
        self.sftp = None
        self.ssh = None

def main():
    pass

if __name__ == '__main__':
    main()
