import sys, socket, os, re, time

class FTPClient():
    def __init__(self, host='localhost', port=21, user='root', passwd='root', parser=False):
        self.controlSock = None
        self.bufSize = 4096
        self.connected = False
        self.loggedIn = False
        self.dataMode = 'PORT'
        self.dataAddr = None
        self.host = host
        self.port = port
        self.user = user
        self.passwd = passwd
        self.parser = parser
        
    def parseReply(self):
        if self.controlSock == None:
            return
        try:
            reply = self.controlSock.recv(self.bufSize).decode('ascii')
        except (socket.timeout):
            return
        else:
            if 0 < len(reply):
                if self.parser:
                    print('<< ' + reply.strip().replace('\n', '\n<< '))
                return (int(reply[0]), reply)
            else: # Server disconnected
                self.connected = False
                self.loggedIn = False
                self.controlSock.close()
                self.controlSock = None
                
    def connect(self, host=None, port=None):
        if self.controlSock != None: # Close existing socket first
            self.connected = False
            self.loggedIn = False
            self.controlSock.close()
        self.controlSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        if host is None:
            host = self.host
        if port is None:
            port = self.port
        self.controlSock.connect((host, port))
        if self.parseReply()[0] <= 3:
            self.connected = True
            self.controlSock.settimeout(1.0) # Timeout 1 second
            
    def login(self, user=None, passwd=None):
        if not self.connected:
            return
        user = self.user if not user else user
        passwd = self.passwd if not passwd else passwd
        if not user: user = 'anonymous'
        if not passwd: passwd = ''
        if user == 'anonymous' and passwd in ('', '-'):
            passwd = passwd + 'anonymous@'
            
        self.loggedIn = False
        self.controlSock.send(('USER %s\r\n' % user).encode('ascii'))
        if self.parseReply()[0] <= 3:
            self.controlSock.send(('PASS %s\r\n' %passwd).encode('ascii'))
            if self.parseReply()[0] <= 3:
                self.loggedIn = True
                
    def quit(self):
        if not self.connected:
            return
        self.controlSock.send(b'QUIT\r\n')
        self.parseReply()
        self.connected = False
        self.loggedIn = False
        self.controlSock.close()
        self.controlSock = None
        
    def pwd(self):
        if not self.connected or not self.loggedIn:
            return
        self.controlSock.send(b'PWD\r\n')
        self.parseReply()
        
    def cwd(self, path):
        if not self.connected or not self.loggedIn:
            return
        self.controlSock.send(('CWD %s\r\n' % path).encode('ascii'))
        self.parseReply()
        
    def help(self):
        if not self.connected or not self.loggedIn:
            return
        self.controlSock.send(b'HELP\r\n')
        self.parseReply()
        
    def type(self, t):
        if not self.connected or not self.loggedIn:
            return
        self.controlSock.send(('TYPE %s\r\n' % t).encode('ascii'))
        self.parseReply()
        
    def pasv(self):
        self.controlSock.send(b'PASV\r\n')
        reply = self.parseReply()
        if reply[0] <= 3:
            m = re.search(r'(\d+),(\d+),(\d+),(\d+),(\d+),(\d+)', reply[1])
            self.dataAddr = (m.group(1) + '.' + m.group(2) + '.' + m.group(3) + '.' + m.group(4), int(m.group(5)) * 256 + int(m.group(6)))
            self.dataMode = 'PASV'
            
    def nlst(self):
        if not self.connected or not self.loggedIn:
            return
        if self.dataMode != 'PASV': # Currently only PASV is supported
            return
        dataSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        dataSock.connect(self.dataAddr)
        self.controlSock.send(b'NLST\r\n')
        time.sleep(0.5) # Wait for connection to set up
        dataSock.setblocking(False) # Set to non-blocking to detect connection close
        while True:
            try:
                data = dataSock.recv(self.bufSize)
                if len(data) == 0: # Connection close
                    break
                print(data.decode('ascii').strip())
            except (socket.error): # Connection closed
                break
        dataSock.close()
        self.parseReply()
        
    def retr(self, filename, remotepath='', localpath='', changename=''):
        if not self.connected or not self.loggedIn:
            return
        if self.dataMode != 'PASV': # Currently only PASV is supported
            return
        dataSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        dataSock.connect(self.dataAddr)
        if remotepath is not '':
            self.cwd(remotepath)
        self.controlSock.send(('RETR %s\r\n' % filename).encode('ascii'))
        if changename is not '':
            filename = changename
        if localpath is not '':
            filename = os.path.join(localpath,filename)
        fileOut = open(filename, 'wb')
        time.sleep(0.5) # Wait for connection to set up
        dataSock.setblocking(False) # Set to non-blocking to detect connection close
        while True:
            try:
                data = dataSock.recv(self.bufSize)
                if len(data) == 0: # Connection close
                    break
                fileOut.write(data)
            except (socket.error): # Connection closed
                break
        fileOut.close()
        dataSock.close()
        self.parseReply()
        
    def stor(self, filename, remotepath='', localpath=''):
        if not self.connected or not self.loggedIn:
            return False
        if self.dataMode != 'PASV': # Currently only PASV is supported
            return False

        dataSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM, 0)
        dataSock.connect(self.dataAddr)
        if remotepath is not '':
            self.cwd(remotepath)
        self.controlSock.send(('STOR %s\r\n' % filename).encode('ascii'))
        if localpath is not '':
            filename = os.path.join(localpath,filename)
        dataSock.send(open(filename, 'rb').read())
        dataSock.close()
        self.parseReply()
        return True
        
    def downloadAllFile(self, remotepath='', localpath=''):
        file_list = self.nlst
        if remotepath is not '':
            self.cwd(remotepath)
        for filename in file_list:
            self.retr(filename, '', localpath)
    
    def autoUpload(self, filename='', remotepath='', localpath=''):
        if not filename:
            return False
        try:
            self.connect()
            self.login()
            self.pasv()
            self.stor(filename, remotepath, localpath)
            self.quit()
            return True
        except Exception as e:
            print e
            return False
        finally:
            self.quit()
        
    def autoDownload(self, filename='', remotepath='', localpath='', changename=''):
        if not filename:
            return False
        try:
            self.connect()
            self.login()
            self.pasv()
            self.retr(filename, remotepath, localpath, changename)
            return True
        except Exception as e:
            print e
            return False
        finally:
            self.quit()
        
    def autoDownloadAll(self, remotepath='', localpath=''):
        try:
            self.connect()
            self.login()
            self.pasv()
            self.downloadAllFile(remotepath, localpath)
        except Exception as e:
            print e
            return False
        finally:
            self.quit()

def main():
    ftpclient = FTPClient('192.168.0.21', 21, 'root', 'root', True)
    #ftpclient.autoUpload('test_upload.txt')
    #ftpclient.autoDownload('test_download.txt')
    ftpclient.connect()
    ftpclient.login()
    ftpclient.pasv()
    ftpclient.nlst()

if __name__ == '__main__':
    main()
