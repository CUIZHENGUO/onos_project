#!/usr/bin/env python
# -*- coding: utf8 -*-

import os
import sys
import time
import signal
import tempfile
import threading
import netifaces
import bottle
from onosEmFunction import onosFunction
try:
    import fcntl
except ImportError:
    fcntl = None

from em2vnmf import emRegAck, emRest, emSetting, emNotify
from util.emTCPServer import TCPServer
from util.emLog import getLog
from util import *

LOCK_PATH = os.path.join(tempfile.gettempdir(), "lock")

OS_WIN = False
if 'win32' in sys.platform.lower():
    OS_WIN = True
else:
    os.environ['LD_LIBRARY_PATH'] = "/usr/local/lib/"
    os.environ['PATH'] = os.environ['PATH'] + ":/usr/local/sbin/"
    #set onos conf
    onosFunction.setONOSEnv()

class SingleInstance:
    def __init__(self):
        self.fh = None
        self.is_running = False
        self.do_magic()

    def do_magic(self):
        if OS_WIN:
            try:
                if os.path.exists(LOCK_PATH):
                    os.unlink(LOCK_PATH)
                self.fh = os.open(LOCK_PATH, os.O_CREAT | os.O_EXCL | os.O_RDWR)
            except EnvironmentError as err:
                if err.errno == 13:
                    self.is_running = True
                else:
                    raise
        else:
            try:
                self.fh = open(LOCK_PATH, 'w')
                fcntl.lockf(self.fh, fcntl.LOCK_EX | fcntl.LOCK_NB)
            except EnvironmentError as err:
                if self.fh is not None:
                    self.is_running = True
                else:
                    raise

    def clean_up(self):
        # this is not really needed
        try:
            if self.fh is not None:
                if OS_WIN:
                    os.close(self.fh)
                    os.unlink(LOCK_PATH)
                else:
                    fcntl.lockf(self.fh, fcntl.LOCK_UN)
                    self.fh.close() # ???
                    os.unlink(LOCK_PATH)
        except Exception as err:
            pass

def startEmLog():
    emSetting.startLogging()
    
def stopEmLog():
    emSetting.stopLogging()
    
def readEmConf(config_file='em.conf"'):
    try:
        config = emSetting.initConfig()
        return config
    except Exception as e:
        raise e
    
def chkIntfIp():
    def addressInNetwork(ip, net):
       "Is an address in a network"
       print ip
       print net
       print "==================="
       ipaddr = int(''.join([ '%02x' % int(x) for x in ip.split('.') ]), 16)
       netstr, bits = net.split('/')
       netaddr = int(''.join([ '%02x' % int(x) for x in netstr.split('.') ]), 16)
       mask = (0xffffffff << (32 - int(bits))) & 0xffffffff
       
       print ipaddr
       print netaddr
       print mask
       return (ipaddr & mask) == (netaddr & mask)

    try:
        for interface in netifaces.interfaces():
            if interface == 'lo': continue
            addr = netifaces.ifaddresses(interface)
            if netifaces.AF_INET in addr and addressInNetwork(addr[netifaces.AF_INET][0]['addr'], emSetting.SUBNET_MANAGER):
                return addr[netifaces.AF_INET][0]['addr']
    except Exception as e:
        raise e

    return None
    
class HTTPServer(threading.Thread):
    def __init__(self, host, port):
        threading.Thread.__init__(self)
        self.host = host
        self.port = port # Port where the listen service must be started
        self.url = "http://" + emSetting.config['ema_ip'] + ":" + emSetting.config['ema_port'] + "/em/" + self.host
    def run(self):
        bottle.run(host=self.host, port=self.port, debug=False, quiet=True)

def signal_handler(signal, frame):
    emSetting.emlog.saveLog(getLog('EM[%s] Got signal [%d] and Stop !' % (emSetting.config['vnf_name'] ,signal)))
    data = {'status': 'Stop', 'reason': 'EM[%s] Got signal [%d] and Stop !' % (emSetting.config['vnf_name'], signal)}
    emNotify.emNotifyVnf(data)
    sys.exit(1)
  
def main():
    print "start main"


    try:
        si = SingleInstance()
        if si.is_running:
            sys.exit("This app is already running!")
            return
        

        config = readEmConf()
        emlog = emSetting.initLogging()
        emSetting.startLogFile()


        #onosFunction.onos_config

        print "install onos"
        onosFunction.installONOS()
        print os.environ['ONOS_APPS']

        signal.signal(signal.SIGINT,  signal_handler)
        signal.signal(signal.SIGTERM, signal_handler)
        signal.signal(signal.SIGILL,  signal_handler)
        signal.signal(signal.SIGFPE,  signal_handler)
        signal.signal(signal.SIGSEGV, signal_handler)
        signal.signal(signal.SIGABRT, signal_handler)
     
        count_check = 0
        em_ip = chkIntfIp()
        interval = int(config['mgt_if_check_interval'])
        while em_ip is None and count_check < int(config['max_mgt_if_check']):
            count_check += 1
            emlog.saveLog(getLog('Check Interface Fail, Try [%s]' % count_check))
            time.sleep(interval)
            em_ip = chkIntfIp()
        if count_check == int(config['max_mgt_if_check']):
            emlog.saveLog(getLog("Management Interface Fail !"))
            return
        emlog.saveLog(getLog("Management Interface Success [%s]" % em_ip))
        
        config['em_ip'] = em_ip
        em_port = config['em_port']
        httpthread = HTTPServer(em_ip, em_port)
        httpthread.setDaemon(True)
        httpthread.start() # em start listening
        print "start listening"
        while True:
            if len(emSetting.DEFAULT_SAVE_DIR) == 0:
                emSetting.emlog.saveLog(getLog('Waiting check_vnf_request message'))
            time.sleep(3)
    except (KeyboardInterrupt):
        httpthread._Thread__stop()
        emlog.saveLog(getLog("Ctrl+C pressed... Shutting Down"))
        exit("Ctrl+C pressed... Shutting Down")
    except (SystemExit):
        pass
    except Exception as e:
        print e
        #emlog.saveLog(getLog("Err (%s), program quit!" % e))
    finally:
        #pass
        si.clean_up()

if __name__ == '__main__':
    main()
