import json
import requests
from em2vnmf import emSetting
from util.emLog import getLog
from onosEmFunction import onosFunction

def emNotifyVnf(data):
    em_ip = emSetting.config['em_ip']
    ema_ip = emSetting.config['ema_ip']
    ema_port = emSetting.config['ema_port']
    url = "http://" + ema_ip + ":" + ema_port + "/em/" + em_ip
    
    status = data['status']
    reason = data['reason']
    tid = emSetting.tid if emSetting.tid else ''
    data = {'tid': tid, 'status': status, 'reason': reason, 'method': 'vnf_notify'}
    
    log_rev = "****************************************\n"
    log_rev += "VNF Send Notify - Msg send to VNFM\n"
    log_rev += json.dumps(data, indent=2)
    log_rev += "\n****************************************"
    onosFunction.logMessage("emNotifyVnf", log_rev)
    emSetting.emlog.saveLog(getLog(log_rev, True))
    
    try:
        r = requests.post(url, json.dumps(data))
        r.raise_for_status()
        result = r.status_code
        return True
    except Exception as e:
        emSetting.emlog.saveLog(getLog('Could not send notify , http result : ', e))
        onosFunction.logMessage("emNotifyVnf-Exception", getLog(e))
        return False
