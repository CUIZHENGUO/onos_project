import json
import requests
import time
from threading import Thread
from em2vnmf import emSetting
from util.emLog import getLog
from util.emVnf import checkStatusVnf

default_file = []
def emCheckVnf(data):
    try:
        emSetting.tid = data['tid']
        if data['deflag'] == 'WITHOUT_DEF':
            log_rev = "########################################\n"
            log_rev += "Check VNF: Msg from VNFM (No default config)\n"
            log_rev += json.dumps(data, indent=2)
            log_rev += "\n########################################"
            emSetting.emlog.saveLog(getLog(log_rev, True))
        elif data['deflag'] == 'WITH_DEF':
            log_rev = "########################################\n"
            log_rev += "Check VNF: Msg from VNFM (Default config)\n"
            log_rev += json.dumps(data, indent=2)
            log_rev += "\n########################################"
            emSetting.emlog.saveLog(getLog(log_rev, True))
            
            numOfconfFile = str(data['numOfconfFile'])
            configpathFile = data['configPathFile'] # list of config path and file
            for i in range(0, int(numOfconfFile)):
                #configPath = configpathFile[i]['configPath']
                configFileName = configpathFile[i]['configFileName']
                if configFileName != '*':
                    default_file.append(configpathFile[i]['configFileName'])
                if configpathFile[i]['configPath'] not in emSetting.DEFAULT_SAVE_DIR:
                    emSetting.DEFAULT_SAVE_DIR.append(configpathFile[i]['configPath'])
            emSetting.setDefaultConfigFile(default_file)
            #emSetting.DEFAULT_SAVE_DIR = configpathFile[0]['configPath'] if len(configpathFile) > 0 else './'
            emSetting.LOAD_SCENARIO_PATH = emSetting.DEFAULT_SAVE_DIR
            emSetting.emlog.saveLog(getLog("Default dir config: %s" % ','.join(emSetting.DEFAULT_SAVE_DIR)))
        result = 'Success'
    except Exception as e:
        result = 'Fail'
        emSetting.emlog.saveLog(getLog('Cannot recieve ack from vnfm (%s)' % e))

    status = checkStatusVnf()
    data_result = {'status': status, 'result': result}
    return data_result
    
def waitRevMsgFromVnfm(url, data, rev_ok, rev_data):
    try:
        rev_data = requests.post(url, json.dumps(data))
        rev_ok = True
    except:
        rev_data = None
        rev_ok = True

def emRegAckVnf():
    em_ip = emSetting.config['em_ip']
    ema_ip = emSetting.config['ema_ip']
    ema_port = emSetting.config['ema_port']
    vnfm_ip = emSetting.config['vnfm_ip']
    url = "http://" + ema_ip + ":" + ema_port + "/em/" + em_ip
    data = {'em_ip': em_ip, 'status': 'Inst', 'method': 'Registration', 'ema_ip': ema_ip, 'vnfm_ip' : vnfm_ip}
    
    log_send = "########################################\n"
    log_send += "Registration: Msg Send to VNFM\n"
    log_send += json.dumps(data, indent=2)
    log_send += "\n########################################"
    emSetting.emlog.saveLog(getLog(log_send, True))

    try:
        r = requests.post(url, json.dumps(data))
        ack = r.json()      
        if ack['deflag'] == 'WITHOUT_DEF':  # check whether EM uses default config file
            log_rev = "****************************************\n"
            log_rev += "Registration - Msg Reply from VNFM (No default config)\n"
            log_rev += json.dumps(ack, indent=2)
            log_rev += "\n****************************************"
            #emSetting.DEFAULT_SAVE_DIR = None
        elif ack['deflag'] == 'WITH_DEF':
            numOfconfFile = ack['numOfconfFile']
            configpathFile = ack['configPathFile']
            for i in range(0, numOfconfFile):
                if configpathFile[i]['configFileName'] != '*':
                    default_file.append(configpathFile[i]['configFileName'])
                if configpathFile[i]['configPath'] not in emSetting.DEFAULT_SAVE_DIR:
                    emSetting.DEFAULT_SAVE_DIR.append(configpathFile[i]['configPath'])
            #emSetting.DEFAULT_SAVE_DIR = configpathFile[0]['configPath'] if len(configpathFile) > 0 else './'
            emSetting.LOAD_SCENARIO_PATH = emSetting.DEFAULT_SAVE_DIR
            emSetting.emlog.saveLog(getLog("Default dir config: %s" % ','.join(emSetting.DEFAULT_SAVE_DIR)))
            
            log_rev = "****************************************\n"
            log_rev += "Registration - Msg Reply from VNFM (Default config)\n"
            log_rev += json.dumps(ack, indent=2)
            log_rev += "\n****************************************"
        
        emSetting.setDefaultConfigFile(default_file)
        emSetting.emlog.saveLog(getLog(log_rev, True))
        return True
    except Exception as e:
        emSetting.emlog.saveLog(getLog('Cannot recieve ack from vnfm (%s)' % e))
    return False
