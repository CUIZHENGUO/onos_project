import json
import bottle
import datetime
from em2vnmf import emRegAck, emSetting
from vnfm2em import emStart, emStop, emSave, emChange, emLoadScenario, onosManager
from util.emLog import getLog
from util import emVnf
from onosEmFunction import onosFunction


@bottle.route('/listen', method='PUT')
def emRestRead():
    try:

        data = json.load(bottle.request.body)
        if 'method' in data:
            req_time = str(datetime.datetime.utcnow())
            if data['method'] == 'start':
                result, reason = emStart.emStartVnf(data)
                res_time = str(datetime.datetime.utcnow())
                status = emVnf.checkStatusVnf()
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method':'start', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': 'start', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}
                    
                log_send = "######################################\n"
                log_send += "VNF Start - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n#####################################"
                emSetting.emlog.saveLog(getLog(log_send, True))
                onosFunction.logMessage("Rest Start", log_send)
                return json.dumps(ack)
            elif data['method']=='stop':
                result, reason  = emStop.emStopVnf(data)
                res_time = str(datetime.datetime.utcnow())
                status = emVnf.checkStatusVnf()
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method': 'stop', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': 'stop', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}
                    
                log_send = "######################################\n"
                log_send += "VNF Stop - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n######################################"
                emSetting.emlog.saveLog(getLog(log_send, True))
                
                return json.dumps(ack)

            elif data['method']=='save':
                result, reason = emSave.emSaveVnf(data)
                res_time = str(datetime.datetime.utcnow())
                status = emVnf.checkStatusVnf()
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method': 'save', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': 'save', 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}
                    
                log_send = "######################################\n"
                log_send += "VNF Config Save - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n######################################"
                emSetting.emlog.saveLog(getLog(log_send, True))

                return json.dumps(ack)
            elif data['method'] == 'regist_ack':
                result = emRegAck.emRegAckVnf()
                return json.dumps({})  
            elif data['method'] == 'check_vnf_request':
                data_result = emRegAck.emCheckVnf(data)
                res_time = str(datetime.datetime.utcnow())
                em_ip = emSetting.config['em_ip']
                ema_ip = emSetting.config['ema_ip']
                vnfm_ip = emSetting.config['vnfm_ip']
                ack = {'em_ip': em_ip, 'ema_ip': ema_ip, 'vnfm_ip': vnfm_ip, 'method': 'check_vnf_response', 'status': data_result['status'], 'req_time': req_time, 'res_time': res_time, 'result': data_result['result']}
                
                log_rev = "########################################\n"
                log_rev += "Check VNF: Ack to VNFM\n"
                log_rev += json.dumps(ack, indent=2)
                log_rev += "\n########################################"
                emSetting.emlog.saveLog(getLog(log_rev, True))
                return json.dumps(ack)
            # REST API for ONOS Application Management
            elif data['method'] == 'manage_app':

                result, reason  = onosManager.onosAppManage(data)
                res_time = str(datetime.datetime.utcnow())
                status = emVnf.checkStatusVnf()
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status,  'req_time': req_time, 'res_time': res_time, 'result': result}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status,  'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}

                log_send = "######################################\n"
                log_send += "ONOS App Manager - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n######################################"
                emSetting.emlog.saveLog(getLog(log_send, True))
                onosFunction.logMessage("Rest Method", log_send)
                return json.dumps(ack)

            # REST API for check ONOS Application
            elif data['method'] == 'check_app':
                result, reason  = onosManager.onosAppCheck(data)
                res_time = str(datetime.datetime.utcnow())
                onosFunction.logMessage("Rest Method", result + reason )
                status = emVnf.checkStatusVnf()
                onosFunction.logMessage("Rest Method status", status)
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'state': reason}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}
                onosFunction.logMessage("Rest Method", json.dumps(ack))
                log_send = "######################################\n"
                log_send += "ONOS App Checker - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n######################################"
                emSetting.emlog.saveLog(getLog(log_send, True))
                return json.dumps(ack)

            # REST API for ONOS Clustering
            elif data['method'] == 'cluster':
                result, reason  = onosManager.onosCluster(data)
                res_time = str(datetime.datetime.utcnow())
                status = emVnf.checkStatusVnf()
                if result == 'Success':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result}
                elif result == 'Fail':
                    ack = {'tid': data['tid'], 'method': data['method'], 'status': status, 'req_time': req_time, 'res_time': res_time, 'result': result, 'reason': reason}

                log_send = "######################################\n"
                log_send += "ONOS Cluster - ACK to VNFM\n"
                log_send += json.dumps(ack, indent=2)
                log_send += "\n######################################"
                emSetting.emlog.saveLog(getLog(log_send, True))
                return json.dumps(ack)


            else:
                emSetting.emlog.saveLog(getLog("resapi: undefined messgae"))
                status = emVnf.checkStatusVnf()
                ack = {'tid': data['tid'], 'method': data['method'], 'status': status, 'result': 'Fail', 'reason': "Undefined method"}
                return json.dumps(ack)
    except Exception as e:
        onosFunction.logMessage("emRest Exception", getLog(e))
        emSetting.emlog.saveLog(getLog(e))
