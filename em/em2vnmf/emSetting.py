import os

SUBNET_MANAGER = '172.17.0.0/24'
DEFAULT_SAVE_DIR = []

def initConfig(config_file='em.conf'):
    global config
    global start_count
    global vnf_is_running
    global LOAD_SCENARIO_PATH
    global defaultConfigFile
    global backupfile
    global event
    global tid
    
    event = None
    LOAD_SCENARIO_PATH = []
    defaultConfigFile = []
    backupfile = []
    start_count = 0
    vnf_is_running = False
    config = {}
    tid = None
    try:
        with open(config_file, 'r') as f:
            for line in f:
                line = line.strip()
                if not line or line[0] is '#': continue
                line = line.split('=')
                config[line[0].strip()] = line[1].strip() if len(line) > 1 else None
        return config
    except:
        raise

def setDefaultConfigFile(file_list):
    global defaultConfigFile
    for filename in file_list:
        if filename not in defaultConfigFile:
            defaultConfigFile.append(filename)
 
def initLogging():
    from util.emLog import EMLogger
    from os.path import dirname, abspath
    global emlog
    global config
    log_dir = dirname(dirname(abspath(__file__))) + '/log'
    vnf_name = config['vnf_name']
    emlog = EMLogger(log_dir=log_dir, vnf_name=vnf_name)
    return emlog
    
def startLogFile():
    global emlog
    emlog.startLogFile()
    
def stopLogFIle():
    global emlog
    emlog.stopLogFile()
    
def enableLogConsole():
    global emlog
    emlog.enableConsoleLog()
    
def disableLogConsole():
    global emlog
    emlog.disableConsoleLog()
