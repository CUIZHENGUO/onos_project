#!/bin/bash 

# Check the VNFM argument exist or not. 
if [ "$#" -ne 3 ]
  then
    echo "[ERROR]:Fail to create EM, Parameter format is not valid."
    echo "[INFO]First Parameter is VNFM ip."
    echo "[INFO]Second Parameter is EMA IP"
    echo "[INFO]Third Parameter is SUBNET_ID."
else
	VNFM_IP=$1
	EMA_IP=$2
	SUBNET_ID=$3
	perl -pi.old -e "s|vnfm_ip =.*|vnfm_ip = $VNFM_IP|" em.conf
	perl -pi.old -e "s|ema_ip =.*|ema_ip = $EMA_IP|" em.conf
	perl -pi.old -e "s|SUBNET_MANAGER =.*|SUBNET_MANAGER = '$SUBNET_ID'|" em2vnmf/emSetting.py
	python2.7 emMain.py
fi
