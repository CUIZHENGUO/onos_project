"""
Description: ONOS Functions for ONOS EM.
Date: 2017/01/02
Author: zhenguo cui
contact: quejinkook@korea.ac.kr

Function List:
- setONOSEnv(): Set ONOS Running Environment,
                set JAVA_HOME, JRE_HOME and MAVEN_HOME
                ONOS APPS, ONOS Features
- startONOS(): Start ONOS
- stopONOS(): Stop ONOS
- checkONOS(): check ONOS heartbeat
- setClusterInfo(ip_nic, ip_list): set cluster info and restart onos
- manageONOSApp(app_name, activate = true, host = "localhost"): activate/ deactivate for onos application
- checkONOSApp(app_name, host = "localhost")
- copyConfToLocalRepo(): copy config files to conf directory.
"""

import json
import os
import shutil
from subprocess import Popen, PIPE
from em2vnmf import emSetting
from util.emLog import getLog
import onosGenPartitions
from vnfm2em import emStop, emStart




def setONOSApps(apps="drivers,openflow,fwd,proxyarp,mobility"):
    os.environ['ONOS_APPS'] = apps


def setONOSIP(ip="172.17.0.2"):
    os.environ['ONOS_IP'] = ip

def setClusterIPS(ipList="172.17.0.2,172.17.0.3,172.17.0.4"):
    os.environ['ONOS_CLUSTER_IP'] = ipList

def setONOSEnv(config_file='onos.conf'):

    #get parameters
    global onos_config
    onos_config = {}
    try:
        with open(config_file, 'r') as f:
            for line in f:
                line = line.strip()
                if not line or line[0] is '#': continue
                line = line.split('=')
                onos_config[line[0].strip()] = line[1].strip() if len(line) > 1 else None
    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        return 'Fail', e

    os.environ['JAVA_HOME'] = onos_config["JAVA_HOME"]
    os.environ['JRE_HOME'] = onos_config["JRE_HOME"]
    os.environ['MAVEN_HOME'] = onos_config["MAVEN_HOME"]
    os.environ['PATH'] = os.environ['PATH'] + ":" + os.environ['JAVA_HOME'] + "/bin:" + os.environ['JRE_HOME'] + "/bin:" + os.environ['MAVEN_HOME']+"/bin"

    # set default onos variables.
    os.environ['ONOS_BOOT_FEATURES'] = onos_config["ONOS_BOOT_FEATURES"]
    os.environ['ONOS_USER'] = onos_config["ONOS_USER"]
    os.environ['ONOS_NIC'] = onos_config["ONOS_NIC"]
    # set apps and nic, may different when user change onos cluster info, or apps
    setONOSApps(apps = onos_config["ONOS_APPS"])
    setONOSIP(ip = onos_config["ONOS_IP"])
    setClusterIPS(onos_config["ONOS_CLUSTER_IP"])

def installONOS():
    """ Function for install ONOS
    process:
    run onos_install script
    :return: "Success" / "Fail"
    """

    p = Popen(emSetting.config['install_cmd'], shell=True, env=os.environ)
    result = p.wait()
    logMessage("snstallONOS","install is done: \n log:" + str(result))

    return "Success","" if result == 0 else "Fail",result

def startONOS():
    """ Function for start ONOS
    process:
    3 start ONOS
    """

    # check onos is running
    # if checkONOS:
    #    stopONOS(status = True);
    logMessage("startONOS: check JAVA HOME", os.environ['JAVA_HOME'])
    p = Popen(emSetting.config['start_cmd'], shell=True, env=os.environ)
    result = p.wait()
    #os.system(emSetting.config['start_cmd'])
    logMessage("startONOS","start is done")

def stopONOS(status = False):
    """ Function for stop ONOS
    process:
    1 save ONOS variables
    2 stop ONOS
    """
    if not status:
        running = checkONOS()
        if not running:
            logMessage("stopONOS","[error] ONOS is not running, could not stopped!")
            return

    # save ONOS Variables
    saveONOSVariables()
    # stop ONOS
    p = Popen(emSetting.config['kill_cmd'], shell=True)
    p.wait()
    logMessage("stopONOS","stop is done")


def checkONOS():
    """ Function for check ONOS heartbeat
    process:
    1 check onos process is running
    2 return true if is running. else return false
    """
    p = Popen(emSetting.config['check_cmd'], shell=True, stdout= PIPE)
    result, err = p.communicate()
    if(result.split(" ")[0] == "Running"):
        logMessage("CheckONOS-true", result)
        return True
    else:
        logMessage("CheckONOS-false", result)
        return False


def setClusterInfo(ip, ip_list, restart=True):
    """ Function for set Cluster
    Process:
    1 read input value, which contains IP list
    2 create cluster.json file
    3 move to directory
    4 (restart ONOS if 'restart' is true)
    """

    try:
        # stop onos
        emStop.emStopVnfNone()

        # reinstall onos
        p = Popen(emSetting.config['install_cmd'], shell=True)
        result = p.wait()
        logMessage("setClusterInfo","install is done: \n log:" + str(result))

        # set onos_ip
        setONOSIP(ip)
        setClusterIPS(ip_list)
        # read input value
        ips = ip_list.split(",")

        # set system.property
        shutil.copy("/opt/onos_project/system.properties", "system.properties")
        with open("system.properties","a") as f:
            f.write("onos.ip = " + os.environ['ONOS_IP'])
        # move property to target directory.
        shutil.move("system.properties", "/opt/onos_project/onos/apache-karaf-3.0.5/etc/system.properties")
        logMessage("setClusterInfo","move system.property: ")

        # create cluster.json file
        for i in range(0 , len(ips)):
            os.environ["OC[%d]"%(i+1)] = ips[i]
            logMessage("Cluster", "OC Value os.environ['OC%d'%(i+1)]"  + os.environ['OC[%d]'%(i+1)])
        onosGenPartitions.generate_cluster_file('cluster.json',ips)
        logMessage("setClusterInfo","create cluster json")
        # move to directory

        shutil.copy("cluster.json", "/opt/onos_project/onos/config/cluster.json")
        logMessage("Cluster", "move file is done")

        # restart ONOS
        emStart.emStartVnfNone()
        return "success",""

    except Exception as e:
        emSetting.emlog.saveLog(getLog(e))
        logMessage("setClusterInfo Exception",getLog(e))
        return 'Fail', e

def manageONOSApp(app_name, activate = True, host="localhost"):
    """ Function for activate/ deactivate onos application
    Process:
    1 read input value, which contains app name and activate
    2-1 if activate is "true", activate app
    2-2 else activate is "false", deactivate app
    3 call restapi for activting, & deactivating app
    """
    cmd = "onosEmFunction/onos-app " + host
    cmd += " activate" if activate else " deactivate"
    cmd += " " + app_name

    if activate:
        print "Activate"
    else:
        print "DeActivate"

    logMessage("manageONOSApp","app list:" + onos_config["ONOS_APPS"])
    p = Popen(cmd, shell=True, env=os.environ)
    p.wait()
    logMessage("manageONOSApp","command is send\n" + cmd)

def checkONOSApp(app_name, host="localhost"):
    """ Function for activate/ deactivate onos application
    Process:
    1 call onos-app appstatus to get App Status
    2 return result. if it is activate then return "ACTIVE"
    """

    cmd = "onosEmFunction/onos-app " + host
    cmd += " appstatus " + app_name
    p = Popen(cmd, shell=True, env=os.environ, stdout=PIPE)
    logMessage("checkONOSApp","command is send\n" + cmd)

    output, err = p.communicate()
    print output
    output_json = json.loads(output)
    print output_json
    state = output_json["state"]
    print state

    logMessage("checkONOSApp","The app status is \n" + state)
    return state


def copyConfToLocalRepo(local_dir = " /usr/local/onosConf"):
    """ Function for copy config files to conf directory.
    Process:
    1 mv files to /usr/local
    """


    p = Popen(emSetting.config['save_cmd'], shell=True)
    result = p.wait()
    logMessage("copyConfToLocalRepo","copy process result:"+result)

def copyConfToOnos():
    """ Function for copy files to ONOS directory
    Process:
    1 mv files to required directory
    """
    p = Popen(emSetting.config['load_cmd'], shell=True)
    result = p.wait()
    logMessage("copyConfToOnos","copy process result:"+result)

def saveONOSVariables():
    """ Function for save variables of ONOS
    Process:
    1 write data to file
    """

    # save current active apps
    p = Popen(emSetting.config['save_cmd'], shell=True)
    result = p.wait()
    logMessage("saveONOSVariables","save apps is done \n log:" + str(result))

    shutil.move("onos.conf", "onos.conf.backup")
    variable = ""
    variable += "ONOS_BOOT_FEATURES = " + os.environ['ONOS_BOOT_FEATURES'] + "\n"
    variable += "ONOS_USER = " + os.environ['ONOS_USER'] + "\n"
    variable += "ONOS_APPS = " + os.environ['ONOS_APPS'] + "\n"
    variable += "ONOS_IP = " + os.environ['ONOS_IP'] + "\n"
    variable += "ONOS_NIC = " + os.environ['ONOS_NIC'] + "\n"
    variable += "ONOS_CLUSTER_IP = " + os.environ['ONOS_CLUSTER_IP'] + "\n"
    variable += "JAVA_HOME = " + os.environ['JAVA_HOME'] + "\n"
    variable += "JRE_HOME = " + os.environ['JRE_HOME'] + "\n"
    variable += "MAVEN_HOME = " + os.environ['MAVEN_HOME'] + "\n"

    with open("onos.conf","w") as f:
        f.write(variable)
    logMessage("saveONOSvariables","save is done")

def logMessage(function_name, message):
    log_rev = "\n****************************************\n"
    log_rev += "%s - ONOS Function Log Message\n" % (function_name)
    log_rev += message + "\n"
    log_rev += "****************************************"
    emSetting.emlog.saveLog(getLog(log_rev,True))
    print getLog(log_rev,True)
